(function(UI){

    "use strict";

    var flips = [];

    UI.component('flip', {

        defaults: {
            target    : false,
            cls       : 'uk-hidden',
            animation : false,
            duration  : 200
        },

        boot: function(){

            // init code
            UI.ready(function(context) {

                UI.$('[data-uk-flip]', context).each(function() {
                    var ele = UI.$(this);

                    if (!ele.data('flip')) {
                        var obj = UI.flip(ele, UI.Utils.options(ele.attr('data-uk-flip')));
                    }
                });

                setTimeout(function(){

                    flips.forEach(function(flip){
                        flip.getFlips();
                    });

                }, 0);
            });
        },

        init: function() {

            var $this = this;

            this.aria = (this.options.cls.indexOf('uk-hidden') !== -1);


            this.on('click', function(e) {
              if ($this.element.is('a[href="#"]')) {
                  e.preventDefault();
              }
              $this.flip();
            });

            flips.push(this);
        },

        flip: function() {

            this.getFlips();

            if(!this.toflip.length) return;


            this.toflip.toggleClass(this.options.cls);
            UI.Utils.checkDisplay(this.toflip);

            this.toflip[0].scrollIntoView({scrollMode: 'if-needed', block: "nearest", inline: "nearest", behavior: "smooth"})

            this.updateAria();

        },

        getFlips: function() {
            this.toflip = this.options.target ? UI.$(this.options.target):[];
            this.updateAria();
        },

        updateAria: function() {
            if (this.aria && this.toflip.length) {
                this.toflip.not('[aria-hidden]').each(function(){
                    UI.$(this).attr('aria-hidden', UI.$(this).hasClass('uk-hidden'));
                });
            }
        }
    });

})(UIkit2);

jQuery(function () {
  jQuery("#tm-content").animate({ opacity:1 }, 00)
})
